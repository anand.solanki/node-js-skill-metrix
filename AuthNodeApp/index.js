var http = require('http');
var express = require('express');
var bodyParser = require('body-parser');
var config = require('./config/settings.json');
var mongoose = require('mongoose');
var usersModel = require('./models/user');
var authRoutes = require('./routes/auth');
var session = require('express-session');
var flash = require('express-flash');

let app = express();
app.server = http.createServer(app);

mongoose.Promise = global.Promise;
mongoose.connect(config.database);
app.set('view engine', 'ejs');
app.use(function (req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin,Authorization, X-Requested-With, Content-Type, Accept");
	next();
});
app.use(session({ secret: 'secratedata' }));
app.use(express.static("public"));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(flash());
app.use(authRoutes);

app.server.listen(process.env.PORT || config.port, () => {
	console.log(`Started on port ${app.server.address().port}`);
});

module.exports = app;