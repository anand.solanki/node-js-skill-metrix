var authctrl =require('../controllers/authctrl')
var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
    authctrl.login(req, res)
}); 
router.get("/login", function(req, res){
    authctrl.login(req, res)
});
router.post('/login', function(req, res) {
    authctrl.login_a_user(req, res)
});
router.get("/allow", function (req, res) {
    authctrl.get_permission(req, res)
});
router.post("/allow", function (req, res) {
    authctrl.allow_permission(req, res)
});
router.get("/cancel", function (req, res) {
    authctrl.cancel_permission(req, res)
});
router.post("/oauth/token", function (req, res) {
    authctrl.get_token(req, res)
});
router.get("/ifttt/v1/user/info", function (req, res) {
    authctrl.get_user_info(req, res)
});





// router.get('/signup', function(req, res) {
//     authctrl.signup_a_user(req, res)
// });
// router.post('/signup', function(req, res) {
//     authctrl.create_a_user(req, res)
// });
// router.get('/dashboard', function(req, res) {
//     authctrl.list_all_users(req, res)
// });
// router.get('/logout', function(req, res) {
//     authctrl.logout(req, res)
// });
module.exports = router;