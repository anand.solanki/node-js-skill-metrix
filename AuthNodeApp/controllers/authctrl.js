var mongoose = require('mongoose');
var UserCollection = mongoose.model('users');
var int_encoder = require('int-encoder');
var crypto = require('crypto');
var ENCRYPTION_KEY = "xyz123";
var sess;

int_encoder.alphabet();

function ValidateUser(req, res) {
    sess = req.session;
    if (sess.Username) {
        return true;
    } else {
        return false;
    }
}
function encrypt_string(string) {
    var cipher = crypto.createCipher('aes-256-cbc', ENCRYPTION_KEY);
    var crypted = cipher.update(string, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return int_encoder.encode(crypted, 16);
}
function decrypt_string(string) {
    key = int_encoder.decode(string, 16);
    var decipher = crypto.createDecipher('aes-256-cbc', ENCRYPTION_KEY);
    var dec = decipher.update(key, 'hex', 'utf8');
    dec += decipher.final('utf8');
    return dec;
}
function random(max) {
    return Math.floor((Math.random() * max) + 1);
}
function generate_randomized_user_code(username) {
    return encrypt_string(username + ":" + random(100));
}
function decrypt_randomized_user_code(user_code) {
    var user_random = decrypt_string(user_code);

    return user_random.split(":")[0];
}
function generate_auth_code_for_user(username) {
    return generate_randomized_user_code(username);
}


module.exports.login = function (req, res) {
    if (ValidateUser(req, res)) {
        return res.redirect('/dashboard');
    }
    var redirectUrl = req.query.redirectUrl? req.query.redirectUrl:req.flash('redirectUrl');
    if(redirectUrl === null || typeof(redirectUrl)=== 'undefined'){
        return res.json(400  ,{ error: "Bad Request" });
    }
    return res.render('../views/login.ejs', { message: req.flash('message'),redirectUrl:redirectUrl});
};

module.exports.login_a_user = function (req, res) {
    sess = req.session;
    var new_user = req.body;
    UserCollection.find({ Username: new_user.Username, Password: new_user.Password }, function (err, Userlist) {
        if (err)
            res.send(err);
        if (Userlist.length > 0) {

            var code = generate_auth_code_for_user(new_user.Username);
            //return res.redirect(new_user.redirectUrl + "?code=" + encodeURIComponent(code));
            //sess.Username = new_user.Username;
            req.flash('code', code);
            req.flash('redirectUrl', new_user.redirectUrl);
            return res.redirect('/allow');
        } else {
            req.flash('message', "Invalid username or password");
            req.flash('redirectUrl',new_user.redirectUrl);
            return res.redirect('/');
        }
    });
};

module.exports.get_permission = function (req, res) {
    return res.render('../views/permission.ejs', { code:req.flash('code'),redirectUrl: req.flash('redirectUrl')});
}

module.exports.allow_permission =  function (req, res) {
    return res.redirect(req.body.redirectUrl + "?code=" + encodeURIComponent(req.body.code));
};

module.exports.cancel_permission = function (req, res) {
    return res.redirect(req.query.redirectUrl);
}

module.exports.get_token = function (req, res) {
    try {
        var username = decrypt_randomized_user_code(req.body.code);
        UserCollection.find({ Username:username}, function (err, Userlist) {
            if (err)
                res.send(err);
            if (Userlist.length > 0) { 
                res.json({
                    token_type: "bearer",
                    access_token: generate_randomized_user_code(username)
                });
            } else{
                return res.json(401, { error: "Invalid code" });
            }
        });
    }
    catch (e) {
        return res.json(401, { error: "Invalid code" });
    }
    
};

module.exports.get_user_info = function (req, res) {
    var bearer_token = req.header("Authorization").split(" ")[1];
    try {
        var username = decrypt_randomized_user_code(bearer_token);
        UserCollection.find({ Username:username}, function (err, Userlist) {
            if (err)
                res.send(err);
            if (Userlist.length > 0) { 
                res.json({"Username": Userlist[0].Username });
            } else{
                return res.json(401, { error: "Invalid access token" });
            }
        });
    }
    catch (e) {
        return res.json(401, { error: "Invalid access token" });
    }
};

module.exports.signup_a_user = function (req, res) {
    if (ValidateUser(req, res)) {
        return res.redirect('/dashboard');
    }
    res.render('../views/signup.ejs', { message: "" });
};

module.exports.create_a_user = function (req, res) {
    var new_user = new UserCollection(req.body);
    UserCollection.find({ Username: new_user.Username }, function (err, Userlist) {
        if (err)
            res.send(err);
        if (Userlist.length > 0) {
            return res.render('../views/signup.ejs', { message: "'That username is already taken.'" });
        } else {
            new_user.save(function (err, user) {
                if (err)
                    res.send(err);
                return res.redirect('/');
            });
        }
    });
};

module.exports.list_all_users = function (req, res) {
    if (!ValidateUser(req, res)) {
        return res.redirect('/');
    }
    UserCollection.find({}, function (err, Userlist) {
        if (err)
            res.send(err);
        res.render('../views/dashboard.ejs', {
            users: Userlist
        });
    });
};

module.exports.logout = function (req, res) {
    req.session.destroy(function (err) {
        if (err) {
            console.log(err);
        } else {
            res.redirect('/');
        }
    });
}
