var mongoose = require('mongoose');
let Schema = mongoose.Schema;

var UserSchema = new Schema({
  Username: {
    type: String,
    Required: [true,'Kindly enter Username']
  },
  Password: {
    type: String,
    Required: [true,'Kindly enter Password']
  }
}); 
module.exports = mongoose.model('users', UserSchema);