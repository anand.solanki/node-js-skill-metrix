var mqtt = require('mqtt');
var fs = require('fs');
var path = require('path');
var cryption = require('./cryption');

const topic = 'presence';
// var lastWill = { will: { topic: topic,
//     payload: 'Device Offline' , 
//     },
//     clientId: '1',
//     port: 1883,
//     clean: false,
//     keepalive: 5
// };
// var client = mqtt.connect('mqtt://localhost', lastWill);
var client = mqtt.connect('mqtts://localhost:8883' , {
    username: 'test',
    password: 'test',
    keepalive: 60,
    clean: true,
    clientId: '2',
    qos: 0,
    key: fs.readFileSync(path.resolve('/home/test/nodejs/idp_test/nodejs/mqtt-examples/sample/certs/client.key')),
    cert: fs.readFileSync(path.resolve('/home/test/nodejs/idp_test/nodejs/mqtt-examples/sample/certs/client.crt')),
    ca: fs.readFileSync(path.resolve('/home/test/nodejs/idp_test/nodejs/mqtt-examples/sample/certs/ca.crt')),
    rejectUnauthorized: true,
    checkServerIdentity: function (host, cert) {
        if (host != cert.subject.CN) {
            console.log("host and cert with are incorrect");
            return 'Incorrect server identity';// Return error in case of failed checking.
        } else {
            console.log("host and cert with correct");
        }
    }
});
var options = { retain: false, qos: 1 };
var i = 1;
client.on('connect', function () {
    if (i == 1) {

        client.subscribe(topic);
        console.log("Connection eastablished! And Subscribed to topic: " + topic);
    }
    else {
        console.log("Reconnected!");
    }
});

client.on('message', function (topic, message) {
    // message is Buffer
    console.log(`Encrypted Message: ${message.toString()}`);
    let decodedString = cryption.decrypt("1345",message.toString());
    console.log(`Decoded Message: ${decodedString}`);
});

client.on('error', function(err) {
    console.log(err);
});

setInterval(() => {
    i = 2;
    client.end();
    client.reconnect();
}, 5000);