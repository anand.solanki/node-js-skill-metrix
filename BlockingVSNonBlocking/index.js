const fs = require('fs');

fs.readFile('file.txt', (err, data) => {
  if (err) throw err;
  console.log('Non Blocking File :: ==>',data);

});

const data = fs.readFileSync('file.txt'); // blocks here until file is read
console.log('Blocking File :: ==>',data);


