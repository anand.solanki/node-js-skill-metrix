const path = require('path');
process.stdout.write("\u001b[2J\u001b[0;0H");
console.log(" ====================================== basename ======================================");
console.log(`basename of "/home/temp/myfile.html" is "${path.basename("/home/temp/myfile.html")}"`);
console.log(`using win32 : basename of "/home/temp/myfile.html" is "${path.win32.basename("/home/temp/myfile.html")}"`);
console.log(`using posix : basename of "/home/temp/myfile.html" is "${path.posix.basename("/home/temp/myfile.html")}"`);
console.log(`remove .ext : basename of "/home/temp/myfile.html" is "${path.basename("/home/temp/myfile.html", '.html')}"`);

console.log(" ====================================== delimiter ======================================");
console.log(`Path = ${process.env.PATH}`);
console.log(`Delimiter = ${path.delimiter}`);
console.log(`Split By Delimiter = ${process.env.PATH.split(path.delimiter)}`);

console.log(" ====================================== dirname ======================================");
console.log(`Dir name of "/home/temp/myfile.html" is "${path.dirname('/home/temp/myfile.html')}"`);

console.log(" ====================================== extname ======================================");
console.log(`Ext name of "/home/temp/myfile.html" is "${path.extname('/home/temp/myfile.html')}"`)

console.log(" ====================================== format ======================================");
console.log(path.format({
  root: '/ignored',
  dir: '/home/temp',
  base: 'myfile.html'
}));
console.log(path.format({
  root: '/home/temp/',
  name: 'myfile',
  ext:'.html'
}));

console.log(" ====================================== isAbsolute ======================================");
console.log(`Path "/home/temp/" isAbsolute : ${path.isAbsolute('/home/temp/')}`);
console.log(`Path "/home/temp/.." isAbsolute : ${path.isAbsolute('/home/temp/..')}`);
console.log(`Path "temp/" isAbsolute : ${path.isAbsolute('temp/')}`);
console.log(`Path "." isAbsolute : ${path.isAbsolute('.')}`);

console.log(" ====================================== join ======================================");
console.log(`Join Path "path.join('/home', 'temp', 'myfile.html')" is like "${path.join('/home', 'temp', 'myfile.html')}"`);
console.log(`Join Path "path.join('/home', 'temp', 'myfile.html','..')" is like "${path.join('/home', 'temp', 'myfile.html','..')}"`);

console.log(" ====================================== normalize ======================================");
console.log(`Normalize Path "path.normalize('/foo/bar//baz/asdf/quux/..')" is like "${path.normalize('/foo/bar//baz/asdf/quux/..')}"`);
 

console.log(" ====================================== parse ======================================");
console.log(`Parse path "path.parse('/home/temp/myfile.html')" like`,path.parse('/home/temp/myfile.html'));

console.log(" ====================================== posix ======================================");
console.log(path.posix);

console.log(" ====================================== relative ======================================");
console.log(`Relative path from "/data/orandea/test/aaa" to "/data/orandea/impl/bbb" is : "${path.relative('/data/orandea/test/aaa', '/data/orandea/impl/bbb')}"`);
 

console.log(" ====================================== resolve ======================================");
console.log(`Resolve path "path.resolve(static_files/png/', '../gif/image.gif')" is : "${path.resolve('static_files/png/', '../gif/image.gif')}"`);


console.log(" ====================================== sep ======================================");
console.log(`Path Sep is "${path.sep}"`);

console.log(" ====================================== win32 ======================================");
console.log(`Path win32 is`,path.win32);



