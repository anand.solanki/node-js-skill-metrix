var fs = require("fs");
var zlib = require('zlib');

console.clear();

/*Reading from a Stream*/
var readFile = (fileName)=>{
	var data = '';
	var readerStream = fs.createReadStream(fileName);
	readerStream.setEncoding('UTF8');
	readerStream.on('data', function(chunk) {
	   	data += chunk;
	});
	readerStream.on('end',function(){
		console.log(`Read completed of => ${fileName} =>`,data);
	});
	readerStream.on('error', function(err){
	   	console.log(err.stack);
	});
}

/*Write a Stream*/
setTimeout(()=> {
	var data = 'This is simple example';
	var writerStream = fs.createWriteStream('test2.txt');
	writerStream.write(data,'UTF8');
	writerStream.end();
	writerStream.on('finish', function() {
	   	console.log("Write completed of => test2.txt");
		readFile('test2.txt');
		setTimeout(()=> {
			pipeStream();
		},1000);
	});
	writerStream.on('error', function(err){
		console.log(err.stack);
	});
},1500);


/*Piping the Streams*/
var pipeStream = ()=>{
	var readerStream = fs.createReadStream('test.txt');
	var writerStream = fs.createWriteStream('test2.txt');
	readerStream.pipe(writerStream);
	setTimeout(()=> {
		console.log("Pipe data 'test.txt' => 'test2.txt'");
		readFile('test2.txt');
	},2000);
}

/*Chaining the Streams*/
fs.createReadStream('test.txt')
   .pipe(zlib.createGzip())
   .pipe(fs.createWriteStream('test.txt.gz'));
  
console.log("File Compressed.");

setTimeout(()=>{
	fs.createReadStream('test.txt.gz')
	   .pipe(zlib.createGunzip())
	   .pipe(fs.createWriteStream('test.txt'))
	console.log("File Decompressed.");
	setTimeout(()=>{
		readFile('test.txt');
	},500);
},500);








