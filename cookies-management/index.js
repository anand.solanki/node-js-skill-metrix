var express = require('express');
var cookieParser = require('cookie-parser');
var cookie_name = 'Store cookie';
var port = 3004;

var app = express();
app.use(cookieParser());

//Set Cookie
app.get('/setcookie', function (req, res) {
    res.cookie(cookie_name, 'cookie_value').send('Cookie is set with cookie_value name');
    console.log('Successfully set Cookie');
});

//Get Cookie
app.get('/', function (req, res) {
    console.log('Current Cookies :  ' + cookie_name, req.cookies);
    res.send(`Get Cookie Successfully ::  Current Cookies :  ${cookie_name}`);
});

//Delete Cookie
app.get('/clearcookie', function (req, res) {
    res.clearCookie('cookie_name');
    res.send('Successfully deleted Cookie for cookie_name');
    console.log('Successfully deleted Cookie for cookie_name');
});

var server = app.listen(port, function () {

    var host = server.address().address;
    var port = server.address().port;

    console.log('Example app listening at http://%s:%s', host, port);

});