
const AmazonCognitoIdentity = require('amazon-cognito-identity-js');
const CognitoUserPool = AmazonCognitoIdentity.CognitoUserPool;
const AWS = require('aws-sdk');
const request = require('request');
const jwkToPem = require('jwk-to-pem');
const jwt = require('jsonwebtoken');
global.fetch = require('node-fetch');

const IDENTITYPOOLID = "us-east-1:fb6587e4-a7a9-4a1d-a0e6-9cfdd4ac7932";
const USER_POOL_ID = "us-east-1_eqvqpE3pr";
const CLIENT_ID = "5bk94vs5tdpv7ftfvlp6236k0p";
const USER_NAME = 'harikrushna';
const PASSWORD = 'MyNewPass1!1';
const pool_region = 'us-east-1';
const ATTRIBUTES = {
    Name: "email",
    Value: "harikrushna.moradiya@volansystech.com"
}
const CODE = "896939";

const poolData = {
    UserPoolId: USER_POOL_ID,
    ClientId: CLIENT_ID
};
const userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);

AWS.config.update({ region: pool_region });
function registerUser() {
    var attributeList = [];
    attributeList.push(new AmazonCognitoIdentity.
        CognitoUserAttribute(ATTRIBUTES));
    userPool.signUp(
        USER_NAME,
        PASSWORD,
        attributeList,
        null, (err, result) => {
            if (err) {
                console.log(err);
                return;
            }
            cognitoUser = result.user;
            console.log('user name is ' + cognitoUser.getUsername());
        });
}
//registerUser();

function confirmRegistration() {
    const userData = {
        Username: USER_NAME,
        Pool: userPool
    };
    const cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
    cognitoUser.confirmRegistration(CODE, true, (err, result) => {
        if (err) {
            console.log(err);
            return;
        }
        console.log('call result: ' + result);
    });
}
//confirmRegistration();

function loginUser() {
    var authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails({
        Username: USER_NAME,
        Password: PASSWORD,
    });
    const userData = {
        Username: USER_NAME,
        Pool: userPool
    };
    const cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
    cognitoUser.authenticateUser(authenticationDetails, {
        onSuccess: (result) => {
            // console.log('access token + ' + result.getAccessToken().getJwtToken());
            // console.log('-----------------------------------------------------------');
            // console.log('id token + ' + result.getIdToken().getJwtToken());
            // console.log('-----------------------------------------------------------');
            // console.log('refresh token + ' + result.getRefreshToken().getToken());
            ValidateToken(result.getIdToken().getJwtToken(), result.getAccessToken().getJwtToken());
        },
        onFailure: (err) => {
            console.log(err);
        },
    });
}
loginUser();

function ValidateToken(IdToken, AccessToken) {
    request({
        url: `https://cognito-idp.${pool_region}.amazonaws.com/${poolData.UserPoolId}/.well-known/jwks.json`,
        json: true
    }, (error, response, body) => {
        if (!error && response.statusCode === 200) {
            pems = {};
            var keys = body['keys'];
            for (var i = 0; i < keys.length; i++) {
                //Convert each key to PEM
                var key_id = keys[i].kid;
                var modulus = keys[i].n;
                var exponent = keys[i].e;
                var key_type = keys[i].kty;
                var jwk = { kty: key_type, n: modulus, e: exponent };
                var pem = jwkToPem(jwk);
                pems[key_id] = pem;
            }
            //validate the token
            var decodedJwt = jwt.decode(IdToken, { complete: true });
            if (!decodedJwt) {
                console.log("Not a valid JWT token");
                return;
            }

            var kid = decodedJwt.header.kid;
            var pem = pems[kid];
            if (!pem) {
                console.log('Invalid token');
                return;
            }

            jwt.verify(IdToken, pem, (err, payload) => {
                if (err) {
                    console.log("Invalid Token.");
                } else {
                    console.log("Valid Token.");
                    // console.log(payload);
                    getCredentials(IdToken);
                    getUserData(AccessToken);
                }
            });
        } else {
            console.log("Error! Unable to download JWKs");
        }
    });
}

function getCredentials(IdToken) {
    const key = `cognito-idp.${pool_region}.amazonaws.com/${poolData.UserPoolId}`
    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: IDENTITYPOOLID,
        Logins: {
            [key]: IdToken
        }
    });
    AWS.config.credentials.get((err) => {
        if (!err) {
            console.log('--------------------------------');
            console.log('AccessKeyId: ',  AWS.config.credentials.accessKeyId );
            console.log('SecretAccessKey: ',  AWS.config.credentials.secretAccessKey );
            console.log('--------------------------------');
        } else {
            console.log('ERROR: ' + err);
        }
    });
}

function getUserData(AccessToken) {
    const cognitoIdentityServiceProvider = new AWS.CognitoIdentityServiceProvider();
    var params = {
        AccessToken
    };
    cognitoIdentityServiceProvider.getUser(params, (err, data) => {
        if (err) {
            console.log(err, err.stack);
        }
        else {
            console.log('--------------------------------');
            console.log(data);
            console.log('--------------------------------');
        }
    });
}