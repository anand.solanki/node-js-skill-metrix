var net = require('net');
process.stdout.write("\u001b[2J\u001b[0;0H");
var client = net.connect({ port: 8080 }, function () {
    console.log('connected to server!');
});
client.on('data', function (data) {
    console.log(data.toString());
});
client.on('end', function () {
    console.log('disconnected from server');
});

client.write('Data sent by client..');
setTimeout(() => {
    client.end();
}, 1000);