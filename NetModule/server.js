var net = require('net');
process.stdout.write("\u001b[2J\u001b[0;0H");
var server = net.createServer(function (socket) {
    console.log('client connected');

    socket.on('end', function () {
        console.log('client disconnected');
    });
    socket.on('data', function (data) {
        console.log(data.toString());
    });
    socket.write('Hello World!\r\n');
    socket.pipe(socket);
});

server.listen(8080, function () {
    console.log('server is listening');
});