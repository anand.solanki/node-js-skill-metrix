	
/* 
	Awailable Methods
------------------------------------

addListener(event, listener),
on(event, listener),
once(event, listener),
removeListener(event, listener),
removeAllListeners([event]),
setMaxListeners(n),
listeners(event),
emit(event, [arg1], [arg2], [...]),

Class Method
=============
listenerCount(emitter, event),

EVENT
=============
newListener : event,listener,
removeListener : event,listener
------------------------------------
*/

var events = require('events');
var eventEmitter = new events.EventEmitter();
var eventEmitter2 = new events.EventEmitter();
console.clear();

/*Event : newListener*/
eventEmitter2.on('newListener', (event, listener) => {
  console.log(event + " is Added on 'eventEmitter2'");
});
/*Event : removeListener*/
eventEmitter.on('removeListener', (event, listener) => {
  console.log(event + " is removed from 'eventEmitter'");
});


var myListener = () => {
	console.log('eventEmitter :: addListener');
}

var myListener2 = () => {
	console.log('eventEmitter :: addListener');
}

var myListener3 = () => {
	console.log('eventEmitter :: addListener');
}

var myListener4 = () => {
	console.log('eventEmitter :: addListener2');
}

var countListener = () => {
	/*Class Methods : listenerCount*/
	var eventListeners = require('events').EventEmitter.listenerCount(eventEmitter,'addListener');
	console.log(eventListeners + " Listner(s) listening to 'addListener' event");

	var eventListeners = require('events').EventEmitter.listenerCount(eventEmitter2,'addListener2');
	console.log(eventListeners + " Listner(s) listening to 'addListener2' event");

}

/*addListener*/
eventEmitter.addListener('addListener', myListener);

/*on*/
eventEmitter.on('addListener', myListener2);

/*once*/
eventEmitter.once('addListener', myListener3);

/*Class Methods : listenerCount*/
countListener();
eventEmitter.emit('addListener');

/*setMaxListeners*/
eventEmitter2.setMaxListeners(2);
eventEmitter2.addListener('addListener2',myListener4);
eventEmitter2.addListener('addListener2',myListener4);

setTimeout(()=>{
	countListener();
	/*removeListener*/
	eventEmitter.removeListener('addListener',myListener);
	countListener();
	/*removeAllListeners*/
	eventEmitter.removeAllListeners('addListener');
	eventEmitter2.emit('addListener2');
	countListener();
	
	/*listeners*/
	console.log("Listner(s) list from 'addListener2' => ", eventEmitter2.listeners('addListener2'));
},1000);










 

